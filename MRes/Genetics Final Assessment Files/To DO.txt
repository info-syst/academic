Week 2:
1. Report
2. Read all research papers
3. Finish Lectures
----------------------------------------------------

1. A statement of the problem and why it should be solved
2. Reference to and comments upon relevant work by others on the same or similar problems
3. The candidate's ideas and insights for solving the problem and any preliminary results he may have obtained
4. A statement or characterisation of what kind of solution is being sought
5. A plan of action for the remainder of the research; and
6. A rough outline of the thesis itself